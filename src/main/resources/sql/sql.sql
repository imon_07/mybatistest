create database mybatisTestDB;
CREATE TABLE user_info (
  id    INT AUTO_INCREMENT PRIMARY KEY,
  name  VARCHAR(10),
  email VARCHAR(10)
);
