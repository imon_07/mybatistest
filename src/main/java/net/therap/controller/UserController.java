package net.therap.controller;

import net.therap.dao.UserDao;
import net.therap.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * @author imon
 * @since 3/26/14 1:23 PM
 */
@Controller
public class UserController {

    @Autowired
//    @Qualifier("userDaoTemplate")
    @Qualifier("userMapper")
//    @Qualifier("userDao")
    UserDao userDao;

    @RequestMapping(value = "/saveUser", method = RequestMethod.POST)
    public String addContact(@ModelAttribute("user") User user, final RedirectAttributes redirectAttributes) {
        userDao.saveUser(user);
        redirectAttributes.addFlashAttribute("saved", "saved successfully");
        return "redirect:success";
    }

    @RequestMapping("/save")
    public ModelAndView showContacts() {
        return new ModelAndView("saveUser.jsp", "user", new User());
    }

    @RequestMapping(value="success", method=RequestMethod.GET)
    public String showCustomer() {
        return "success.jsp";
    }
}
