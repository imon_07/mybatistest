package net.therap.dao;

import net.therap.model.User;
import org.mybatis.spring.support.SqlSessionDaoSupport;

import java.util.logging.Logger;

/**
 * @author imon
 * @since 3/26/14 12:27 PM
 */
public class UserDaoImpl extends SqlSessionDaoSupport implements UserDao {

    Logger log = Logger.getLogger(UserDaoImpl.class.getName());

    @Override
    public void saveUser(User user) {
        log.info("Saving User Using: SqlSessionDaoSupport");
        getSqlSession().insert("net.therap.dao.UserDao.saveUser", user);
    }
}
