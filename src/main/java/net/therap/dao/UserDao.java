package net.therap.dao;

import net.therap.model.User;

/**
 * @author imon
 * @since 3/26/14 12:27 PM
 */
public interface UserDao {

    void saveUser(User user);
}
