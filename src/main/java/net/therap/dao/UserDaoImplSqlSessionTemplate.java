package net.therap.dao;

import net.therap.model.User;
import org.apache.ibatis.session.SqlSession;

import java.util.logging.Logger;

/**
 * @author imon
 * @since 3/26/14 2:37 PM
 */
public class UserDaoImplSqlSessionTemplate implements UserDao {

    Logger log = Logger.getLogger(UserDaoImpl.class.getName());
    private SqlSession sqlSession;

    public void setSqlSession(SqlSession sqlSession) {
        this.sqlSession = sqlSession;
    }

    @Override
    public void saveUser(User user) {
        log.info("Saving User Using: SqlSessionTemplate");
        sqlSession.insert("net.therap.dao.UserDao.saveUser", user);
    }
}
